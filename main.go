package main

import (
    "database/sql"
    "log"
    "net/http"
    "text/template"
    _"github.com/go-sql-driver/mysql"
)

// Usuario reprenseta o objeto de um cadastro no banco
type Usuario struct {
    ID    int
    Nome  string
	Email string
	Senha string
}

func dbConn() (db *sql.DB) {
    dbDriver := "mysql"
    dbUser := "root"
    dbPass := ""
    dbName := "go_crud"
    db, err := sql.Open(dbDriver, dbUser+":"+dbPass+"@/"+dbName)
    if err != nil {
        panic(err.Error())
    }
    return db
}

var tmpl = template.Must(template.ParseGlob("form/*"))

// Index Lista os usuários
func Index(w http.ResponseWriter, r *http.Request) {
    db := dbConn()
    selDB, err := db.Query("SELECT * FROM usuarios ORDER BY id DESC")
    if err != nil {
        panic(err.Error())
    }
    user := Usuario{}
    res := []Usuario{}
    for selDB.Next() {
        var id int
        var nome, email, senha string
        err = selDB.Scan(&id, &nome, &email, &senha)
        if err != nil {
            panic(err.Error())
        }
        user.ID = id
        user.Nome = nome
		user.Email = email
		user.Senha = senha
        res = append(res, user)
    }
    tmpl.ExecuteTemplate(w, "Index", res)
    defer db.Close()
}

// Show Mostra um usuário específico
func Show(w http.ResponseWriter, r *http.Request) {
    db := dbConn()
    nID := r.URL.Query().Get("id")
    selDB, err := db.Query("SELECT * FROM usuarios WHERE id=?", nID)
    if err != nil {
        panic(err.Error())
    }
    emp := Usuario{}
    for selDB.Next() {
        var id int
        var nome, email, senha string
        err = selDB.Scan(&id, &nome, &email, &senha)
        if err != nil {
            panic(err.Error())
        }
        emp.ID = id
        emp.Nome = nome
		emp.Email = email
		emp.Senha = senha
    }
    tmpl.ExecuteTemplate(w, "Show", emp)
    defer db.Close()
}

// New Mostra o Formulário de cadastro
func New(w http.ResponseWriter, r *http.Request) {
    tmpl.ExecuteTemplate(w, "New", nil)
}

// Edit Mostra o Formulário de edição
func Edit(w http.ResponseWriter, r *http.Request) {
    db := dbConn()
    nID := r.URL.Query().Get("id")
    selDB, err := db.Query("SELECT * FROM usuarios WHERE id=?", nID)
    if err != nil {
        panic(err.Error())
    }
    emp := Usuario{}
    for selDB.Next() {
        var id int
        var nome, email, senha string
        err = selDB.Scan(&id, &nome, &email, &senha)
        if err != nil {
            panic(err.Error())
        }
        emp.ID = id
        emp.Nome = nome
		emp.Email = email
		emp.Senha = senha
    }
    tmpl.ExecuteTemplate(w, "Edit", emp)
    defer db.Close()
}

// Insert Registra um novo usuário no banco de dados
func Insert(w http.ResponseWriter, r *http.Request) {
    db := dbConn()
    if r.Method == "POST" {
        nome := r.FormValue("nome")
		email := r.FormValue("email")
		senha := r.FormValue("senha")
        insForm, err := db.Prepare("INSERT INTO usuarios(nome, email, senha) VALUES(?,?,?)")
        if err != nil {
            panic(err.Error())
        }
        insForm.Exec(nome, email, senha)
        log.Println("INSERT: Nome: " + nome + " | Email: " + email + " | Senha: " + senha)
    }
    defer db.Close()
    http.Redirect(w, r, "/", 301)
}

// Update Atualiza um usuário
func Update(w http.ResponseWriter, r *http.Request) {
    db := dbConn()
    if r.Method == "POST" {
        nome := r.FormValue("nome")
		email := r.FormValue("email")
		senha := r.FormValue("senha")
        id := r.FormValue("uid")
        insForm, err := db.Prepare("UPDATE usuarios SET nome=?, email=?, senha=? WHERE id=?")
        if err != nil {
            panic(err.Error())
        }
        insForm.Exec(nome, email, senha, id)
        log.Println("UPDATE: nome: " + nome + " | email: " + email + " | senha:" + senha)
    }
    defer db.Close()
    http.Redirect(w, r, "/", 301)
}

// Delete Exclui um usuário
func Delete(w http.ResponseWriter, r *http.Request) {
    db := dbConn()
    emp := r.URL.Query().Get("id")
    delForm, err := db.Prepare("DELETE FROM usuarios WHERE id=?")
    if err != nil {
        panic(err.Error())
    }
    delForm.Exec(emp)
    log.Println("DELETE")
    defer db.Close()
    http.Redirect(w, r, "/", 301)
}

func main() {
    log.Println("Server started on: http://localhost:8080")
    http.HandleFunc("/", Index)
    http.HandleFunc("/ver", Show)
    http.HandleFunc("/novo", New)
    http.HandleFunc("/editar", Edit)
    http.HandleFunc("/insert", Insert)
    http.HandleFunc("/update", Update)
    http.HandleFunc("/deletar", Delete)
    http.ListenAndServe(":8080", nil)
}